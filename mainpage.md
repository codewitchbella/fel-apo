# SNAKE!!!

Snake!!! je hra pro MZ_APO kit.

## Zkompilování projektu pro MZ_APO kit

Ke kompilaci používáme cmake.

```
cd slozka/s/projektem
mkdir build
CXX=$(which arm-linux-gnueabihf-g++) cmake ..
make
```

spustitelný soubor pro MZ_APO kit se jmenuje `semestralka`

## Zkompilování projektu pro desktop

Závislosti: SDL2

Ke kompilaci používáme cmake.

```
cd slozka/s/projektem
mkdir build
cmake .. -DDesktop=Yes
make
```

spustitelný soubor se jmenuje `semestralka`

## Struktura projektu

main() je v souboru src/program.cpp, ten však obsahuje pouze
inicializaci jednotlivých modulů a spouští hlavní smyčku screen_loop().

screen_loop() je nekonečná smyčka, která vždy spustí danou obrazovku a poté co
obrazovka zkončí tak zobrazí obrazovku indikovanou návratovou hodnotou obrazovky
předchozí. Soubor src/screen.cpp ve kterém je definována dále
obsahuje funkce pro vykreslování některých jednoduchých obrazovek.

Soubor src/draw.cpp (v třídě Framebuffer) obsahuje základní kreslící rutiny a
zápis na lcd display přípravku (případně do okna pokud aplikace běží na desktopu).

Soubor src/util.cpp obsahuje inicializaci, práci s rotátory a funkce
sleep_ms() a time_us(). Tento soubor obsahuje většinu funkcí závislých na
platformě. Emulace desktop knobů (čtení z klávesnice) se provádí ve funkci
sleep_ms().

Soubor src/net.cpp obsahuje rutiny na komunikaci po síti a
src/net.h definici jednotlivých packetů jako struktury (viz
[komunikační protokol](NetworkProtocol.md))

V souboru src/snake.cpp je třída Game a implementace jednolivých
obrazovek, které využívají vše předešlé pro vytvoření výsledné hry.

## Kam dál

- [Uživatelský manuál](README.md)
- [Síťový protokol](NetworkProtocol.md)
