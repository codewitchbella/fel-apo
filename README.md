# UZIVATELSKY MANUAL

## Kompilace projektu

viz [hlavní stránka dokumentace](mainpage.md)

## Menu

Pomoci libovolneho otocneho tlacitka vyberete a stisknutim potvrdite vyber

![](screenshots/main-menu.png)

## Hra

### Ovladani hry

Had hrace (v hre pro jednoho hrace - cerveny) se ovlada modrym koleckem.
Otoceni doprava - had se otoci doprava
Otoceni doleva - had se otoci doleva

![](screenshots/game.png)

### Cil hry

Cil hry je jednoduchy, zustat poslednim hadem ve hre. Pripadne muzete
skodit jinym hracum.

![](screenshots/you-won.png)

### Konec hry

Konec hry nastane, pokud narazite do tela libovolneho hada, nebo jste
poslednim hadem ve hre.

![](screenshots/you-died.png)

### Hra pro vice hracu

#### Host game

V menu vyberete moznost Host game. Nasledne se Vam zobrazi obrazovka,
kde nastavite jmeno serveru, na ktery se pripoji dalsi hraci. Jmeno
je jiz hahodne vygenerovane z predem daneho seznamu. Pripadne se da
zmenit pomoci zeleneho a modreho kolecka. Zelenym koleckem menite
vybrane pismeno - oznaceno zelene. Modrym pak vybirate, ktere
pismeno budete upravovat. Pro potvrzeni jmena stisknete cervene
kolecko, tim zacnete hostovat hru. Pote, co se pripoji hraci, opet
potvrdite stisknutim cerveneho kolecka a hra muze zacit.

![](screenshots/host-game.png)
![](screenshots/hosting-game-empty.png)
![](screenshots/hosting-game-joined.png)

#### Join game

V menu vyberete moznost Join game. Stejne jako u host game nastavite
Vase jmeno. Po potvrzeni jmena se Vam zobrazi obrazovka, kde
vyberete jmeno hry, kterou chcete hrat. Vyber se provadi cervenym
koleckem. Otocenim listujete a stisknutim potvrdite. Po potvrzeni
se zobrazi na obrazovce waiting a cekate, dokud zakladatel hry
nespusti hru.

![](screenshots/join-game.png)
![](screenshots/choose-a-game.png)
![](screenshots/waiting.png)

Prejeme prijemnou zabavu.
