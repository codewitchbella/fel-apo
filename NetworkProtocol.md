# KOMUNIKACNI PROTOKOL

Format jednotlivych zprav protokolu APO Game of Snake Protocol v 1.0

| Byte offset | Datovy typ | Vyznam                                                     |
|-------------|------------|------------------------------------------------------------|
| 0 ... 3     | uint32_t   | Kontrolni cislo 'AGSP'                                     |
| 4 ... 7     | uint32_t   | Verze protokolu 1.0 0x00010000                             |
| 8 ... 11    | uint32_t   | Typ zpravy (1 banner, 2 join, 3 start, 4 knobs,  5 update) |
| 12 ...      |            | Vlastni data zpravy                                        |

Obsah zpravy pro hostovani hry

| Byte offset | Datovy typ | Vyznam                         |
|-------------|------------|--------------------------------|
| 0 ... 3     | uint32_t   | Kontrolni cislo 'AGSP'         |
| 4 ... 7     | uint32_t   | Verze protokolu 1.0 0x00010000 |
| 8 ... 11    | uint32_t   | Typ zpravy 1 - banner          |
| 12 ... 28   | 16x char   | jmeno hry                      |

Obsah zpravy pro pripojeni se ke hre

| Byte offset | Datovy typ | Vyznam                         |
|-------------|------------|--------------------------------|
| 0 ... 3     | uint32_t   | Kontrolni cislo 'AGSP'         |
| 4 ... 7     | uint32_t   | Verze protokolu 1.0 0x00010000 |
| 8 ... 11    | uint32_t   | Typ zpravy 2 - join            |
| 12 ... 27   | 16x char   | jmeno hry                      |
| 28 ... 43   | 16x char   | jmeno hrace                    |
| 44 ... 47   | Uint32_t   | hodnota rotatoru               |

Obsah zpravy pro zacatek hry

| Byte offset | Datovy typ  | Vyznam                         |
|-------------|-------------|--------------------------------|
| 0 ... 3     | uint32_t    | Kontrolni cislo 'AGSP'         |
| 4 ... 7     | uint32_t    | Verze protokolu 1.0 0x00010000 |
| 8 ... 11    | uint32_t    | Typ zpravy 3 - start           |
| 12 ... 27   | 16x char    | jmeno hry                      |
| 28 ... 31   | uint32_t    | pocet hracu                    |
| 32 ... 159  | 8x 16x char | jmena jednotlivych hracu       |
| 160 ... 287 | 8x uint16_t | barvy jednotlivych hracu       |

Obsah zpravy pro rotatory

| Byte offset | Datovy typ | Vyznam                         |
|-------------|------------|--------------------------------|
| 0 ... 3     | uint32_t   | Kontrolni cislo 'AGSP'         |
| 4 ... 7     | uint32_t   | Verze protokolu 1.0 0x00010000 |
| 8 ... 11    | uint32_t   | Typ zpravy 4 - knobs           |
| 12 ... 27   | 16x char   | jmeno hry                      |
| 28 ... 43   | 16x char   | jmeno hrace                    |
| 44 ... 47   | uint32_t   | hodnota rotatoru               |

Obsah zpravy pro hraci desku

| Byte offset | Datovy typ  | Vyznam                                             |
|-------------|-------------|----------------------------------------------------|
| 0 ... 3     | uint32_t    | Kontrolni cislo 'AGSP'                             |
| 4 ... 7     | uint32_t    | Verze protokolu 1.0 0x00010000                     |
| 8 ... 11    | uint32_t    | Typ zpravy 5 - update                              |
| 12 ... 27   | 16x char    | jmeno hry                                          |
| 28 ... 1563 | 48x32x char | hraci deska (0 = prazdne misto, 1-8 = cislo hrace, |
|             |             | 128 + (1-8)) = hlava hrace                         |

