/*
 * Copyright <2017> <Jakub Skořepa, Klára Jakešová>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "net.h"
#define PORT 5352
#define NET_DEBUG false
namespace {
int sockfd = 0;
}

void net_init()
{
    if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) == -1) {
        perror("socket");
        exit(1);
    }

    int yes=1;
    // allow reuse
    if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)) == -1) {
        perror("setsockopt (SO_REUSEADDR)");
        exit(1);
    }

    if (setsockopt(sockfd, SOL_SOCKET, SO_BROADCAST, &yes, sizeof(yes)) == -1) {
        perror("setsockopt (SO_BROADCAST)");
        exit(1);
    }

    uint32_t timeoutMS = 100;
    struct timeval tv;
    tv.tv_sec = timeoutMS / 1000;
    tv.tv_usec = (timeoutMS % 1000) * 1000;
    if(setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, (const char*) &tv, sizeof(struct timeval)) == -1) {
        perror("setsockopt (timeout)");
        exit(1);
    }

    struct sockaddr_in bindaddr;

    memset(&bindaddr, 0, sizeof(bindaddr));
    bindaddr.sin_family = AF_INET;
    bindaddr.sin_port = htons(PORT);
    bindaddr.sin_addr.s_addr = INADDR_ANY;

    if (bind(sockfd, (struct sockaddr *)&bindaddr, sizeof(bindaddr)) == -1) {
        perror("bind");
        exit(1);
    }
}

void net_broadcast_impl(const void *mess, ssize_t len)
{
    struct sockaddr_in s;

    if (sockfd < 0)
        return;

    memset(&s, 0, sizeof(struct sockaddr_in));
    s.sin_family = AF_INET;
    s.sin_port = (in_port_t)htons(PORT);
    s.sin_addr.s_addr = htonl(INADDR_BROADCAST);

    if(NET_DEBUG) {
        std::cout << "Broadcasting";
        for(int i = 0; i < len; i++) {
            std::cout << " " << (int)(((char*)mess)[i]);
        }
        std::cout << std::endl;
    }

    if (sendto(sockfd, mess, len, 0, (struct sockaddr *)&s, sizeof(struct sockaddr_in)) < 0) {
        perror("sendto");
    }
}

ssize_t net_get_impl(void *msgout, size_t length)
{
    ssize_t l = recv(sockfd, msgout, length, MSG_DONTWAIT);
    if(NET_DEBUG) {
        std::cout << "Recieved (" << l << ")";
        for(int i = 0; i < l; i++) {
            std::cout << " " << (int)(((char*)msgout)[i]);
        }
        std::cout << std::endl;
    }

    return l;
}

bool net_check_packet_base(net_packet_base *base, uint32_t type)
{
    if(base->magic != NET_MAGIC) return false;
    if(ntohl(base->version) > ntohl(NET_VERSION)) return false;
    return base->type == type;
}

net_packet_base::net_packet_base(uint32_t _type)
    :magic(NET_MAGIC), version(NET_VERSION), type(_type) {}

net_packet_banner::net_packet_banner()
:base(NET_TYPE_BANNER)
{

}

net_packet_join::net_packet_join()
:base(NET_TYPE_JOIN)
{

}

net_packet_start::net_packet_start()
:base(NET_TYPE_START)
{

}

net_packet_knobs::net_packet_knobs()
:base(NET_TYPE_KNOBS)
{

}

net_packet_update::net_packet_update()
:base(NET_TYPE_UPDATE)
{
    data.game_over = false;
}

