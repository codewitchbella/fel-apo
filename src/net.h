/*
 * Copyright <2017> <Jakub Skořepa, Klára Jakešová>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <iostream>
#include "util.h" // bool

/**
 * @brief
 * Initializes network communication
 */
void net_init();
/**
 * @brief
 * Implementation of net_broadcast
 *
 * Accepts buffer and its size in bytes
 * @param msg
 * @param len
 */
void net_broadcast_impl(const void *msg, ssize_t len);

/**
 * @brief
 * broadcasts message to local network. Automatically detects its
 * size from its type.
 *
 * @param message
 */
template<typename T>
void net_broadcast(const T& msg) {
    net_broadcast_impl((void*)&msg, sizeof(T));
};

/**
 * @brief
 * Implementation of net_get
 *
 * Accepts buffer and its size
 * @param msgout
 * @param length
 * @return
 */
ssize_t net_get_impl(void *msgout, size_t length);

/**
 * @brief
 * Accepts message from network.
 *
 * Automatically detects its size from its type.
 *
 * @return
 * -1 if error, -2 if recieved message is smaller than msg, size otherwise
 */
template<typename T>
ssize_t net_get(const T& msg) {
    ssize_t r = net_get_impl((void*)&msg, sizeof(T));
    if(r < sizeof(T)) return -2;
    return r;
};

/**
 * Every packet starts with this sequence of bytes
 * AGSP = APO Game of Snake Protocol
 */
#define NET_MAGIC htonl('A' << 24 | 'G' << 16 | 'S' << 8 | 'P') // APO Game of Snake Protocol
/**
 * Version of protocol, 1.0
 */
#define NET_VERSION htonl(0x00010000)
/**
 * Id of packet of type banner
 */
#define NET_TYPE_BANNER htonl(1)
/**
 * Id of packet of type join
 */
#define NET_TYPE_JOIN htonl(2)
/**
 * Id of packet of type start
 */
#define NET_TYPE_START htonl(3)
/**
 * Id of packet of type knobs
 */
#define NET_TYPE_KNOBS htonl(4)
/**
 * Id of packet of type update
 */
#define NET_TYPE_UPDATE htonl(5)

// first 12 bytes of every packet
/**
 * @brief first 12 bytes of every packet
 */
struct net_packet_base {
    uint32_t magic; // should always be NET_MAGIC
    uint32_t version; // we use NET_VERSION and support everything <=
    uint32_t type;

    /**
     * Initializes magic = NET_MAGIC, version = NET_VERSION
     * sets type to equal type
     * @brief net_packet_base
     * @param type
     */
    net_packet_base(uint32_t type);
};

// baner packet
/**
 * @brief banner packet contents
 */
struct net_packet_banner_data {
    char name[16]; // 16th byte is always 0
};

/**
 * @brief contains base and banner contents
 */
struct net_packet_banner {
    net_packet_base base;
    net_packet_banner_data data;

    net_packet_banner();
};

/**
 * @brief join packet contents
 */
struct net_packet_join_data {
    char game_name[16]; // 16th byte is always 0
    char player_name[16]; // 16th byte is always 0
    uint32_t knobs_value;
};

/**
 * @brief contains base and join packet contents
 */
struct net_packet_join {
    net_packet_base base;
    net_packet_join_data data;

    net_packet_join();
};

/**
 * @brief start packet data
 */
struct net_packet_start_data {
    char game_name[16];
    uint32_t player_count;
    char player_names[8][16];
    uint16_t player_colors[8];
};

/**
 * @brief contains base and join packet contents
 */
struct net_packet_start {
    net_packet_base base;
    net_packet_start_data data;

    net_packet_start();
};

/**
 * @brief knobs packet contents
 */
struct net_packet_knobs_data {
    char game_name[16];
    char player_name[16];
    uint32_t knobs;
};

/**
 * @brief contains base and knobs packet contents
 */
struct net_packet_knobs {
    net_packet_base base;
    net_packet_knobs_data data;

    net_packet_knobs();
};

/**
 * @brief update packet contents
 */
struct net_packet_update_data {
    char game_name[16];
    char board[48*32];
    bool game_over;
};

/**
 * @brief contains base and update packet contents
 */
struct net_packet_update {
    net_packet_base base;
    net_packet_update_data data;

    net_packet_update();
};

/**
 * @brief Checks if base is ok (NET_MAGIC, NET_VERSION + type)
 * @param base
 * @param type
 * @return
 */
bool net_check_packet_base(net_packet_base *base, uint32_t type);
