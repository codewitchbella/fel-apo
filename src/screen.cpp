/*
 * Copyright <2017> <Jakub Skořepa, Klára Jakešová>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "screen.h"

#define ARG(name) Framebuffer& name

/**
 * @brief Infinite loop which takes care of state machine to
 * show correct screen
 *
 * Accepts initial screen. Works by calling each function based on
 * id which previous function returned. Takes framebuffer which
 * it passes to called function.
 * @param initial
 * @param fb
 */
void screen_loop(uint16_t initial, ARG(fb))
{
    uint16_t s = initial;
    for(;;) {
        switch(s) {
        case SCREEN_MAIN_MENU: s = screen_main_menu(fb); break;
        case SCREEN_SINGLEPLAYER: s = screen_singleplayer(fb); break;
        case SCREEN_SINGLE_YOU_DIED: s = screen_single_you_died(fb); break;
        case SCREEN_SINGLE_YOU_WON: s = screen_single_you_won(fb); break;
        case SCREEN_MULTI_HOST: s = screen_multi_host_create(fb); break;
        case SCREEN_MULTI_JOIN: s = screen_multi_host_join(fb); break;
        }
    }
}

/**
 * @brief Draws main menu which allows to select correct
 * game mode
 * @return screen id
 */
int screen_main_menu(ARG(fb))
{
    knobs knobs;
    knobs_init(&knobs);
    int command = 0;
    for(;;) {
        fb.fill_black();
        knobs_update(&knobs);

        int8_t v;
        v = knobs.red/4; command += v; knobs.red -= v*4;
        v = knobs.green/4; command += v; knobs.green -= v*4;
        v = knobs.blue/4; command += v; knobs.blue -= v*4;
        if(command < 0) command = 0;
        if(command > 2) command = 2;

        const char *sipka = "<--";
        const char *single = "Hra pro jednoho hrace";
        const char *host = "Host game";
        const char *join = "Join game";
        fb.draw_centered_line(&font_winFreeSystem14x16, 20, "SNAKE!!!", GREEN, 4);
        fb.draw_centered_line(&font_winFreeSystem14x16, 120, single, 0xffff, 2);
        fb.draw_centered_line(&font_winFreeSystem14x16, 160, host, 0xffff, 2);
        fb.draw_centered_line(&font_winFreeSystem14x16, 200, join, 0xffff, 2);

        switch (command) {
            case 0:
                fb.draw_string_scaled(&font_winFreeSystem14x16, 400, 120, sipka, RED, 2);
                fb.draw_centered_line(&font_winFreeSystem14x16, 120, single, RED, 2);
                break;
            case 1:
                fb.draw_string_scaled(&font_winFreeSystem14x16, 320, 160, sipka, RED, 2);
                fb.draw_centered_line(&font_winFreeSystem14x16, 160, host, RED, 2);
                break;
            case 2:
                fb.draw_string_scaled(&font_winFreeSystem14x16, 320, 200, sipka, RED, 2);
                fb.draw_centered_line(&font_winFreeSystem14x16, 200, join, RED, 2);
                break;
            default:
                break;
        }
        if(knobs.red_pressed | knobs.green_pressed | knobs.blue_pressed) {
            while(knobs.red_pressed | knobs.green_pressed | knobs.blue_pressed) {sleep_ms(10); knobs_update(&knobs);}
            switch(command) {
                case 0: return SCREEN_SINGLEPLAYER;
                case 1: return SCREEN_MULTI_HOST;
                case 2: return SCREEN_MULTI_JOIN;
            }
        }
        fb.write_to_lcd();
        sleep_ms(10);
    }
}

/**
 * Waits until knob is pressed
 * @brief await_press
 */
void await_press()
{
    knobs knobs;
    while(1) {
        knobs_update(&knobs);
        if(knobs.red_pressed) {
            while(knobs.red_pressed) {
                knobs_update(&knobs);
                sleep_ms(1);
            }
            return;
        }
        if(knobs.green_pressed) {
            while(knobs.green_pressed) {
                knobs_update(&knobs);
                sleep_ms(1);
            }
            return;
        }
        if(knobs.blue_pressed) {
            while(knobs.blue_pressed) {
                knobs_update(&knobs);
                sleep_ms(1);
            }
            return;
        }
        sleep_ms(10);
    }
}

/**
 * Shows you died text. Does not redraw last shown screen.
 * @brief screen_single_you_died
 * @return
 */
int screen_single_you_died(ARG(fb))
{
    fb.draw_centered_line(&font_winFreeSystem14x16, 20, "You died!", RED, 4);
    fb.write_to_lcd();
    await_press();
    return SCREEN_MAIN_MENU;
}

/**
 * Shows you won text. Does not redraw last shown screen.
 * @brief screen_single_you_won
 * @return
 */
int screen_single_you_won(ARG(fb))
{
    fb.draw_centered_line(&font_winFreeSystem14x16, 20, "You won!", RED | GREEN, 4);
    fb.write_to_lcd();
    await_press();
    return SCREEN_MAIN_MENU;
}
