/*
 * Copyright <2017> <Jakub Skořepa, Klára Jakešová>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef __DRAW_H__
#define  __DRAW_H__

#include "font_types.h"
#include "mzapo_parlcd.h"
// global variables
extern unsigned char *parlcd_mem_base;
#ifdef DESKTOP
#include <SDL2/SDL.h>
extern SDL_Surface * surface;
extern SDL_Texture * texture;
extern SDL_Renderer * renderer;
#endif

/**
 * Width of the lcd screen
 */
#define WIDTH 480
/**
 * Height of the lcd screen
 */
#define HEIGHT 320

/**
 * Returns 565 representation of color
 */
#define RGB(r,g,b) (((r & 0b11111000) << 8) | ((g & 0b11111100) << 3) | ((b & 0b11111000) >> 3))
/**
 * Returns red part of color in range of 0 to 255
 */
#define GET_RED(color) ((color >> 8) & 0b11111000)
/**
 * Returns green part of color in range of 0 to 255
 */
#define GET_GREEN(color) ((color >> 3) & 0b11111100)
/**
 * Returns blue part of color in range of 0 to 255
 */
#define GET_BLUE(color) ((color << 3) & 0b11111000)
/**
 * Red color
 */
#define RED RGB(255, 0, 0)
/**
 * Green color
 */
#define GREEN RGB(0, 255, 0)
/**
 * Blue color
 */
#define BLUE RGB(0, 0, 255)

/**
 * @brief Surface which can be drawn on
 */
class Framebuffer {
private:
    uint16_t fb[WIDTH][HEIGHT];
public:
    /**
     * Draw rectangle to framebuffer
     * @arg x1, y1 ... position of top-left corner
     * @arg x2, y2 ... position of bottom-right corner
     * @arg color ... color
     */
    void draw_rectangle(int x1, int y1, int x2, int y2, uint16_t color);

    /**
     * Draw circle to framebuffer
     * @arg x1, y1 ... position of top-left corner
     * @arg x2, y2 ... position of bottom-right corner
     * @arg color ... color
     */
    void draw_circle(int centerX, int centerY, int radius, uint16_t color);

    /**
     * Draw character to framebuffer
     * @arg font ... text font
     * @arg posX, posY ... position of top-left corner
     * @arg char_id ... ASCII code
     * @arg color ... color
     * @returns width
     */
    unsigned char draw_character(font_descriptor_t* font, int posX, int posY, char char_id, uint16_t color);

    /**
     * Draw character with size option to framebuffer
     * @arg font ... text font
     * @arg posX, posY ... position of top-left corner
     * @arg char_id ... ASCII code
     * @arg color ... color
     * @arg scale ... text size
     * @returns width
     */
    unsigned char draw_character_scaled(font_descriptor_t* font, int posX, int posY, char char_id, uint16_t color, uint8_t scale);

    /**
     * Draw whole text to framebuffer, but can't add new row
     * @arg font ... text font
     * @arg posX, posY ... position of top-left corner
     * @arg *s ... text string
     * @arg color ... color
     * @returns width
     */
    int draw_string(font_descriptor_t* font, int posX, int posY, const char *s, uint16_t color);

    /**
     * Draw whole text with size option to framebuffer, but can't add new row
     * @arg font ... text font
     * @arg posX, posY ... position of top-left corner
     * @arg *s ... text string
     * @arg color ... color
     * @arg scale ... text size
     * @returns width
     */
    int draw_string_scaled(font_descriptor_t* font, int posX, int posY, const char *s, uint16_t color, uint8_t scale);

    /**
     * Returns width of string
     * @arg font ... text font
     * @arg *s ... text string
     * @arg scale ... text size
     * @returns width
     */
    static uint16_t string_width(font_descriptor_t* font, const char *s, uint8_t scale);

    /**
     * Draw whole text centered with size option to framebuffer, but can't add new row
     * @arg font ... text font
     * @arg posX, posY ... position of top-left corner
     * @arg *s ... text string
     * @arg color ... color
     * @arg scale ... text size
     * @returns width
     */
    void draw_centered_line(font_descriptor_t* font, int posY, const char *s, uint16_t color, uint8_t scale);

    /**
     * Paint whole framebuffer with black color
     * @arg fb ... framebuffer
     */
    void fill_black();

    /**
     * Show framebuffer in a window
     * @arg fb ... framebuffer
     */
    void write_to_lcd();
};
#endif //  __DRAW_H__
